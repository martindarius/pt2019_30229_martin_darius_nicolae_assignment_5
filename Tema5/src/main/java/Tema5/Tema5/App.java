package Tema5.Tema5;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, ParseException
    {
    	String fileName = "Activities.txt";
    	Stream<String> activitati=Files.lines(Paths.get(fileName));
    	
    	
    	//1
    	
    	List<String[]> lista=activitati.map(x->x.split("\t\t"))
    			.collect(Collectors.toList());
    	
    	List<MonitoredData> listM=lista.stream()
    			.map(x->new MonitoredData(x[0],x[1],x[2]))
    			.collect(Collectors.toList());
    	
    	//2
    	
       long nrZile=listM.stream()
    			.map(x->x.getStartTime())
    			.map(x->x.split("-"))
    			.map(x->x[2])
    			.map(x->x.split(" "))
    			.map(x->x[0])
    			.distinct()
    			.count();
    	System.out.println("Numarul de zile care apar: "+ nrZile);
    	
    	//3
    	
    	Map<String,Integer>map=listM.stream().map(x->x.getActivity()).collect(Collectors.groupingBy(Function.identity(),Collectors.summingInt(e->1)));
    	System.out.println("Cerinta 3:");
    	for(Entry en:map.entrySet()) {
    		System.out.println(en.getKey()+" "+en.getValue());
    	}
    	
    	//4
    	
    	Map<String,Map<String,Integer>> map2=listM.stream()
    			.collect(Collectors.groupingBy(MonitoredData::getDays,Collectors.groupingBy(MonitoredData::getActivity,Collectors.summingInt(e->1))));
        System.out.println("Cerinta 4:");
       for(Entry<String,Map<String,Integer>> en:map2.entrySet()) {
    	   System.out.println(en.getKey());
    	   for(Entry<String,Integer> en2:en.getValue().entrySet()) {
    		   System.out.println(en2.getKey()+" "+en2.getValue());
    	   }
       }
       
      //5 
       System.out.println("Cerinta 5");
       listM.stream().map(a->a.getActivity()+" "+a.getTime()/3600+":"+a.getTime()%3600/60+":"+a.getTime()%60).forEach(System.out::println);
       
       
       //6
       Map<String,Integer> map3=listM.stream().collect(Collectors.groupingBy(x->x.getActivity(),Collectors.summingInt(x->x.getTime())));
       System.out.println();
       System.out.println("Cerinta 6:");
       for(Entry en:map3.entrySet()) {
    	   int h=(int)en.getValue()/3600;
    	   int m=(int)en.getValue()%3600/60;
    	   int s=(int)en.getValue()%60;
   		System.out.println(en.getKey()+" "+h+":"+m+":"+s);
   	}
       
    }
    
}
