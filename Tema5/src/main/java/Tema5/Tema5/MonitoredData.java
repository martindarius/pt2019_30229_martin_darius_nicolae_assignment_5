package Tema5.Tema5;

public class MonitoredData {
	private String startTime;
	private String endTime;
	private String activity;

	public MonitoredData(String startTime, String endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getDays() {
		String days;
		days=this.getStartTime().substring(8,10);
		return days;
	}
	
	public int getTime() {
		int time;
		int endhours=Integer.parseInt(this.getEndTime().substring(11,13));
		int endminutes=Integer.parseInt(this.getEndTime().substring(14,16));
		int endseconds=Integer.parseInt(this.getEndTime().substring(17,19));
		int starthours=Integer.parseInt(this.getStartTime().substring(11,13));
		int startminutes=Integer.parseInt(this.getStartTime().substring(14,16));
		int startseconds=Integer.parseInt(this.getStartTime().substring(17,19));
		
		int endday=Integer.parseInt(this.getEndTime().substring(8, 10));
		int startday=Integer.parseInt(this.getStartTime().substring(8, 10));
		
		//System.out.println(endhours+" "+endminutes+" "+endseconds);
		if(endday==startday)
			time=endhours*3600+endminutes*60+endseconds-(starthours*3600+startminutes*60+startseconds);
		else
			time=endhours*3600+endminutes*60+endseconds-(starthours*3600+startminutes*60+startseconds)+24*3600;
		return time;
	}
	
}
